package Strings;

import java.util.HashSet;
import java.util.Scanner;

public class Soal06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan kalimat: ");
		String kalimat = input.nextLine().replaceAll(" ", "").toLowerCase();
		
		String hasil = "";

		for (char i = 'a'; i <= 'z'; i++) {
			if (kalimat.indexOf(i) != -1) {
				hasil += i;
			}
		}
	
		if (hasil.length() == 26) {
			System.out.println("Pangram");
		} else {
			System.out.println("Bukan Pangram");
		}
	}
}
