package Strings;

import java.util.Scanner;

public class Soal03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		int x = input.nextInt();
		String code = input.next();
		int s = input.nextInt();

		char[] tempArr = code.toCharArray();
		String result = "";

		for (int i = 0; i < tempArr.length; i++) {
			if (Character.isUpperCase(tempArr[i])) {
				char ch = (char) (((int) tempArr[i] + s - 65) % 26 + 65);
				result += ch;
			} else {
				char ch = (char) (((int) tempArr[i] + s - 97) % 26 + 97);
				result += ch;
			}
		}
		System.out.println("Text	: " + code);
        System.out.println("Shift	: " + s);
        System.out.println("Cipher	: " + result);
	}

}
