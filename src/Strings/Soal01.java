package Strings;

import java.util.Arrays;
import java.util.Scanner;

public class Soal01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Input kata = ");
		String s = input.nextLine();

		char[] tempArr = s.toCharArray();
		
		int hasil = 1;

		for (int i = 0; i < tempArr.length; i++) {
			if (Character.isUpperCase(tempArr[i])) {
				hasil++;
			}
		}
		System.out.println(hasil);
	}
}
