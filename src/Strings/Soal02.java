package Strings;

import java.util.Scanner;

public class Soal02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		int panjang = input.nextInt();
		String pass = input.next();
		char[] tempArr = pass.toCharArray();

		int nomor = 0;
		int lowerCase = 0;
		int upperCase = 0;
		int spesialKarakter = 0;
		int hasil = 0;

		for (int i = 0; i < pass.length(); i++) {
			int ascii = tempArr[i];
			if (ascii >= 48 && ascii <= 57) {
				nomor++;
			} else if (ascii >= 65 && ascii <= 90) {
				upperCase++;
			} else if (ascii >= 97 && ascii <= 122) {
				lowerCase++;
			} else {
				spesialKarakter++;
			}
		}

		if (nomor == 0) {
			hasil++;
		}
		if (lowerCase == 0) {
			hasil++;
		}
		if (upperCase == 0) {
			hasil++;
		}
		if (spesialKarakter == 0) {
			hasil++;
		}
		if (pass.length() + hasil > 6) {
			System.out.println(hasil);
		}else {
			System.out.println(hasil + (6 - (pass.length() + hasil)));
		}
	}
}
