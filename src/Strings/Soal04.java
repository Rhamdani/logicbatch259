package Strings;

import java.util.Scanner;

public class Soal04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
        String S = input.next().toUpperCase();
        int Z = 0;
        
        for(int i = 0; i < S.length(); i++)
        {
            if(i % 3 == 1)
            {
                if(S.charAt(i) != 'O')
                {
                    Z++;
                }
            }
            else
            {
                if(S.charAt(i) != 'S')
                {
                    Z++;
                }
            }
        }
        System.out.println(Z);
    }
}
