package Strings;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


public class Soal10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input =new Scanner(System.in);
		
		Set<Character> s1Set = new HashSet<>();
		Set<Character> s2Set = new HashSet<>();
		
		System.out.print("Masukan kata pertama : ");
		String s1 = input.nextLine();
		for (char ch  : s1.toCharArray()) {
			s1Set.add(ch);
		}
		System.out.print("Masukan kata kedua : ");
		String s2 = input.nextLine();
		for (char ch : s2.toCharArray()) {
			s2Set.add(ch);
		}
		s1Set.retainAll(s2Set);
		if (!s1Set.isEmpty()) {
			System.out.println("YES");
		}else {
			System.out.println("NO");
		}
	}

}
