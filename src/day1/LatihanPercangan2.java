package day1;

import java.util.Scanner;

public class LatihanPercangan2 {

	public static void main(String[] args) {
		
//		Latihan 3!
		Scanner input = new Scanner(System.in);
		
		System.out.print("Belanja : ");
		int belanja = input.nextInt();
		System.out.print("Onkos Kirim : ");
		int onkir = input.nextInt();
		
		int diskonOnkir = 0;
		int diskonBelanja = 0;
		int totalBelanja = 0;
		
		if (belanja >= 100000) {
			diskonOnkir = 10000;
			diskonBelanja = 20000;
			totalBelanja = belanja + onkir - diskonOnkir - diskonBelanja;
			System.out.println("--------------------");
		}else if (belanja >= 50000) {
			diskonOnkir = 10000;
			diskonBelanja = 10000;
			totalBelanja = belanja + onkir - diskonOnkir - diskonBelanja;
			System.out.println("--------------------");
		}else if (belanja >= 30000) {
			diskonOnkir = 5000;
			diskonBelanja = 5000;
			totalBelanja = belanja + onkir - diskonOnkir - diskonBelanja;
			System.out.println("--------------------");
		}
		System.out.println("Belanja : " + belanja);
		System.out.println("Onkos Kirim : " + onkir);
		System.out.println("Diskon Ongkir : " + diskonOnkir);
		System.out.println("Diskon Belanja : " + diskonBelanja);
		System.out.println("Total Belanja : " + totalBelanja);

	}

}
