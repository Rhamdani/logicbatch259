package day2;

import java.util.Scanner;

public class Soal06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan nilai n = ");

		int n = input.nextInt();
		int x = 1;
		int z = 2;

		int[] tempArrayInteger = new int[n];

		for (int i = 0; i < tempArrayInteger.length; i++) {
			tempArrayInteger[i] = x;
			if (i == z) {
				System.out.print("* ");
				z += 3;
			} else {
				System.out.print(tempArrayInteger[i] + " ");
			}
			x += 4;
		}
	}
}
