package day6;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Scanner;

public class Soal01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Waktu masuk : ");
		String masuk = input.nextLine().toLowerCase();

		System.out.print("Waktu keluar : ");
		String keluar = input.nextLine().toLowerCase();

		DateFormat formatAwal = new SimpleDateFormat("dd/MM/yyyy hh:mm");

		Date jamMasuk = null;
		Date jamKeluar = null;

		try {
			jamMasuk = formatAwal.parse(masuk);
			jamKeluar = formatAwal.parse(keluar);
			double diff = jamKeluar.getTime() - jamMasuk.getTime();
			double diff2 = diff / (60 * 60 * 1000);
			int sumParse = (int) Math.ceil(diff2);
			int sumPembayaran = sumParse * 3000;
			System.out.println(sumPembayaran);
		} catch (ParseException z) {
			z.printStackTrace();
		}
	}

}
