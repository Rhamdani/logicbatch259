package day6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

public class Soal03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("masukkan tanggal masuk : ");
		String inputTanggalMulai = input.nextLine();

		System.out.print("masukkan lama bootcamp : ");
		int lama = input.nextInt();
		input.nextLine();

		System.out.print("masukkan tanggal libur : ");
		String[] libur = input.nextLine().split(",");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

		String nextDate = "";
		try {

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateFormat.parse(inputTanggalMulai));

			int tempHitungLibur = 0;

			int i = 0;
			while (i < lama + tempHitungLibur) {
				calendar.add(Calendar.DATE, 1);
				int day = calendar.getTime().getDay();
				int dateofHoliday = calendar.getTime().getDate();

				if ((day == 6) || (day == 0)) {
					tempHitungLibur++;
				}

				int j = 0;
				while (j < libur.length) {
					if (dateofHoliday == Integer.parseInt(libur[j])) {
						tempHitungLibur++;
					}
					j++;
				}
				i++;
			}

			System.out.println("total libur = " + (tempHitungLibur + libur.length));
			nextDate = dateFormat.format(calendar.getTime());
			System.out.println(nextDate);

		} catch (ParseException x) {
			x.printStackTrace();
		}

	}

}
