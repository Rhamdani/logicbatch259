package day6;

import java.util.Scanner;

public class Soal04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Input kata = ");
		String kata = input.nextLine();

		char[] tempArr = kata.toCharArray();

		String result = "";

		for (int i = tempArr.length - 1; i >= 0; i--) {
			result += tempArr[i];
		}

		if (result.equals(kata)) {
			System.out.println("Yes");
		}else {
			System.out.println("No");
		}
	}
}
