package day6;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Soal02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan Tanggal Pinjam : ");
		String pinjam = input.nextLine();
		
		System.out.print("Lama peminjaman : ");
		int lamaPinjam = input.nextInt();
		input.nextLine();
		
		System.out.print("Masukan Tanggal Kembali : ");
		String kembali = input.nextLine();

		DateFormat formatAwal = new SimpleDateFormat("dd/MM/yyyy");

		Date hariPeminjaman = null;
		Date hariPengembalian = null;

		try {
			hariPeminjaman = formatAwal.parse(pinjam);
			hariPengembalian = formatAwal.parse(kembali);
			double differenceTime = hariPengembalian.getTime() - hariPeminjaman.getTime();
			double differenceConvertToDay = differenceTime / (24 * 60 * 60 * 1000);
			if (differenceConvertToDay > lamaPinjam) {
				int rubahDouble = (int)(differenceConvertToDay);
				System.out.println((rubahDouble - lamaPinjam) * 500);
			} else {
				System.out.println("Tepat waktu");
			}
		} catch (ParseException z) {
			z.printStackTrace();
		}
	}
}
