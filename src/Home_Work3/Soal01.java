package Home_Work3;

import java.util.*;

public class Soal01 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan hasil inputan : ");
		int nilai = input.nextInt();

		int arrNilai[] = new int[nilai];

		double median = 0;
		double mean = 0;
		double modus = 0;
		double hasil = 0;

		System.out.println();
		for (int i = 0; i < nilai; i++) {
			System.out.print("Nilai ke-" + (i + 1) + " : ");
			arrNilai[i] = input.nextInt();
			hasil += arrNilai[i];
		}

		System.out.println();
		Arrays.sort(arrNilai);
		System.out.println("Nilai setelah di urutkan : ");
		for (int i : arrNilai) {
			System.out.print(i + " ");
		}

		if (arrNilai.length % 2 == 0) {
			median = ((double) arrNilai[arrNilai.length / 2] + arrNilai[(arrNilai.length / 2) - 1]) / 2;
		} else {
			median = arrNilai[arrNilai.length / 2];
		}

		for (int i = 0; i < arrNilai.length; i++) {
			double count = 0;
			for (int j = 0; j < arrNilai.length; j++) {
				if (arrNilai[i] == arrNilai[j]) {
					count++;
				} else if (count > modus) {
					modus = count;
					modus = arrNilai[i];
				}
			}
		}

		mean = hasil / nilai;

		System.out.println();
		System.out.println("Nilai Mean   adalah : " + mean);
		System.out.println("Nilai Median adalah : " + median);
		System.out.println("Nilai Modus  adalah : " + modus);
	}

}
