package day4;

import java.util.Iterator;
import java.util.Scanner;

public class LatString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		String s = "Xsis Academy";
		
//		Untuk memecah String ke dalam Array
		String[] tempSplit = s.split("");
		

//		Untuk memecah String per karakter ke dalam Array
		char[]tempArr = s.toCharArray();
		
		System.out.println(tempArr);
		

		String result = "";
		for (int i = 0; i < tempSplit.length; i++) {
			result += tempSplit[i];
		}
		System.out.println(result);
	}

}
