package PreTest;

import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

public class Soal07 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan jumlah kartu : ");
		int kartu = input.nextInt();
		String yesNo = "";
		do {
			System.out.print("Jumlah tawaran : ");
			int tawaran = input.nextInt();
			System.out.print("Pilih kotak, A atau B : ");
			String pilih = input.next();

			Random angka = new Random();
			int hasilA = 0;
			int hasilB = 0;

			for (int i = 0; i < 10; i++) {
				hasilA = angka.nextInt(10);
			}
			for (int i = 0; i < 10; i++) {
				hasilB = angka.nextInt(10);
			}

			if (pilih.equals("A") && hasilA > hasilB) {
				kartu = kartu + tawaran;
				System.out.println("Kotak A = " + hasilA);
				System.out.println("Kotak B = " + hasilB);
				System.out.println("You Win");
				System.out.println("kartu yang anda punya = " + kartu);
			} else if (pilih.equals("A") && hasilA < hasilB) {
				kartu = kartu - tawaran;
				System.out.println("Kotak A = " + hasilA);
				System.out.println("Kotak B = " + hasilB);
				System.out.println("You Lose");
				System.out.println("kartu yang anda punya = " + kartu);
			} else if (pilih.equals("B") && hasilA < hasilB) {
				kartu = kartu + tawaran;
				System.out.println("Kotak A = " + hasilA);
				System.out.println("Kotak B = " + hasilB);
				System.out.println("You Win");
				System.out.println("kartu yang anda punya = " + kartu);
			} else if (pilih.equals("B") && hasilA > hasilB) {
				kartu = kartu - tawaran;
				System.out.println("Kotak A = " + hasilA);
				System.out.println("Kotak B = " + hasilB);
				System.out.println("You Lose");
				System.out.println("kartu yang anda punya = " + kartu);
			}
			System.out.println("Mau main lagi ? YES / NO");
			yesNo = input.next();
			
		}while(kartu > 0 && yesNo.equals("YES"));
	}
}
