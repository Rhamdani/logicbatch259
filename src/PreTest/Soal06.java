package PreTest;

import java.util.Scanner;

public class Soal06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan PIN       : ");
		int pin = input.nextInt();
		if (pin == 123456) {
			System.out.print("Uang yang disetor : ");
			long uangSetor = input.nextLong();
			System.out.println("1. Antar Rekening <======> 2. Antar Bank");
			System.out.print("Pilihan Transfer  : ");
			int transfer = input.nextInt();

			long rekening = 0;
			long hasil = 0;
			long uangTranfer = 0;

			switch (transfer) {
			case 1:
				System.out.print("Masukan rekening tujuan  : ");
				rekening = input.nextLong();
				if (rekening < 1000000000) {
					System.out.println("Nomor rekening harus 10 digit");
				}
				System.out.print("Masukan nominal transfer : ");
				uangTranfer = input.nextLong();
				hasil = uangSetor - uangTranfer;
				break;
			case 2:
				System.out.print("Masukan kode bank        : ");
				String kodeBang = input.next();
				System.out.print("Masukan rekening tujuan  : ");
				rekening = input.nextLong();
				if (rekening < 1000000000) {
					System.out.println("Nomor rekening harus 10 digit");
				}
				System.out.print("Masukan nominal transfer : ");
				uangTranfer = input.nextLong();
				hasil = uangSetor - uangTranfer - 7500;
				break;
			default:
				System.out.println("Pilihan tidak ada");
			}
			if (hasil > 0) {
				System.out.println("Transaksi berhasil, saldo anda saat ini Rp." +hasil);
			} else {
				System.out.println("Saldo tidak mencukupi");
			}
		} else {
			System.out.println("PIN salah");
		}
	}
}
