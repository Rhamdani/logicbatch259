package PreTest;

import java.util.Arrays;
import java.util.Scanner;

public class Soal09 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan Sinyal : ");
		String sinyal = input.nextLine().toUpperCase();

		String[] tempSplit = sinyal.split("");
		String result = "";

		int gunung = 0;
		int lembah = 0;

		for (int i = 0; i < tempSplit.length; i += 2) {
			result = tempSplit[i] + tempSplit[i + 1];
			if (result.equals("NT")) {
				gunung += 1;
			} else if (result.equals("TN")) {
				lembah += 1;
			}
		}
		System.out.println("Total gunung yang di lewati : " + gunung);
		System.out.println("Total lembah yang di lewati : " + lembah);
	}
}
