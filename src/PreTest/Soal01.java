package PreTest;

import java.util.Scanner;

public class Soal01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Input Nilai : ");
		int nilai = input.nextInt();

		int ganjil = 1;
		int genap = 2;

		int[] tempArrayGanjil = new int[nilai];
		int[] tempArrayGenap = new int[nilai];

		for (int i = 0; i < tempArrayGanjil.length; i++) {
			tempArrayGanjil[i] = ganjil;
			if (tempArrayGanjil[i] <= nilai) {
				System.out.print(tempArrayGanjil[i] + " ");
				ganjil += 2;
			} else {
				ganjil += 0;
			}
		}

		System.out.println();

		for (int i = 0; i < tempArrayGenap.length; i++) {
			tempArrayGenap[i] = genap;
			if (tempArrayGenap[i] <= nilai) {
				System.out.print(tempArrayGenap[i] + " ");
				genap += 2;
			} else {
				genap += 0;
			}
		}
	}

}
