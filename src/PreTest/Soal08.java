package PreTest;

import java.util.Arrays;
import java.util.Scanner;

public class Soal08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Input panjang deret : ");
		int deret = input.nextInt();

		int genap = 0;
		int ganjil = 1;
		
		int [] deretGenap = new int[deret];
		int [] deretGanjil = new int[deret];
		int [] hasil = new int[deret];
		
		for (int i = 0; i < deretGenap.length; i++) {
			deretGenap[i] += genap; 
			genap += 2;
		}
		
		for (int i = 0; i < deretGanjil.length; i++) {
			deretGanjil[i] += ganjil; 
			ganjil += 2;
		}
		
		for (int i = 0; i < hasil.length; i++) {
			hasil[i] =  deretGenap[i] + deretGanjil[i];
			System.out.print(hasil[i] + " ");
		}

	}

}
