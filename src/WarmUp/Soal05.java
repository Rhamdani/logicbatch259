package WarmUp;

import java.util.Scanner;

public class Soal05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan panjang array = ");
		int array = input.nextInt();

		double[] nilaiArray = new double[array];

		double result1 = 0;
		double result2 = 0;
		double result3 = 0;

		for (int i = 0; i < nilaiArray.length; i++) {
			nilaiArray[i] = input.nextInt();
			if (nilaiArray[i] > 0) {
				result1 += 0.166667;
			} else if (nilaiArray[i] < 0) {
				result2 += 0.166665;
			}else {
				result3 += 0.166667;
			}
		}
		System.out.println(result1);
		System.out.println(result2);
		System.out.println(result3);
	}

}
