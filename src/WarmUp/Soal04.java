package WarmUp;

import java.util.Arrays;
import java.util.Scanner;

public class Soal04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan panjang array = ");
		int array = input.nextInt();
		
		int [][] nilaiArray = new int [array][array];
		
		int result1 = 0;
		int result2 = 0;
		int result3 = 0;
		
		System.out.println("masukan nilai : ");
		for (int i = 0; i < nilaiArray.length; i++) {
			for (int j = 0; j < nilaiArray.length; j++) {
				nilaiArray[i][j] = input.nextInt();
			}
			System.out.println();
		}
		result1 = nilaiArray[0][0] + nilaiArray[1][1] + nilaiArray[2][2];
		result2 = nilaiArray[0][2] + nilaiArray[1][1] + nilaiArray[2][0];
		
		result3 = result1-result2;
		
		System.out.println(Math.abs(result3));
	}

}
