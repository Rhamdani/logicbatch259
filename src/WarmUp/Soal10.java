package WarmUp;

import java.util.Scanner;

public class Soal10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		String angka1 = input.nextLine();
		String angka2 = input.nextLine();

		String[] arrSplit1 = angka1.split(" ");
		String[] arrSplit2 = angka2.split(" ");

		int[] alice = new int[arrSplit1.length];
		int[] bob = new int[arrSplit2.length];

		int aliceTotal = 0;
		int bobTotal = 0;

		for (int i = 0; i < alice.length; i++) {
			alice[i] = Integer.parseInt(arrSplit1[i]);
			bob[i] = Integer.parseInt(arrSplit2[i]);
			if (alice[i] > bob[i]) {
				aliceTotal += 1;
			} else if (alice[i] < bob[i]) {
				bobTotal += 1;
			}
		}
		System.out.println(aliceTotal + "-" + bobTotal);
	}
}