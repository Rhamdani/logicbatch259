package WarmUp;

import java.util.Scanner;

public class Soal06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan angka = ");
		int x = input.nextInt();

		String result = "";

		for (int i = 0; i <= x; i++) {
			for (int j = x; j >= 0; j--) {
				if (i < j) {
					result += " ";
				} else {
					result += "#";
				}
			}
			result += "\n";
		}
		System.out.println(result);
	}

}
