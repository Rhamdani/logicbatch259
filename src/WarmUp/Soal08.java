package WarmUp;

import java.util.Scanner;

public class Soal08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Pangjang array = ");
		int x = input.nextInt();
		
		int z = 1;
		int[] tinggi = new int[x];
		int terTinggi = 0;
		int total = 0;

		for (int i = 0; i < tinggi.length; i++) {
			System.out.print("Masukan nilai ke-" + z +" = " );
			tinggi[i] = input.nextInt();
			z++;
			if (tinggi[i] > terTinggi) {
				terTinggi = tinggi[i];
			}
		}
		for (int i = 0; i < tinggi.length; i++) {
			if (terTinggi == tinggi[i]) {
				total++;
			}
		}
		System.out.println("Total = "  + total);
	}

}
