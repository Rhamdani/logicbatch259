package WarmUp;

import java.io.*;
import java.util.*;

public class Soal09 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        long tambah = 0;
        
        for(int i = 0; i < n; i++){
            tambah += in.nextLong();
        }
        
        System.out.print(tambah);
        in.close();    
    }
}