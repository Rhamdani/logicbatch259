package WarmUp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Soal02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan Waktu : ");
		String x = input.nextLine().toLowerCase();

		DateFormat formatAwal = new SimpleDateFormat("hh:mm:ssaa");
		DateFormat formatAkhir = new SimpleDateFormat("HH:mm:ss");
		 
		try {
			Date date = formatAwal.parse(x);
			String output = formatAkhir.format(date);
			System.out.println(output);
		} catch (ParseException z) {
			z.printStackTrace();
		}
	}

}
