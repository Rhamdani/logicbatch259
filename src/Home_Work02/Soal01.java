package Home_Work02;

import java.util.Scanner;

public class Soal01 {

	public static void main(String[] args) {
		// Fibonacci 2
		Scanner input = new Scanner(System.in);

		System.out.println("Coba input angka : ");
		int n = input.nextInt();

		int[] x = new int[n];
		x[0] = 1;
		x[1] = 1;
		for (int i = 2; i < n; i++) {
			x[i] = x[i - 1] + x[i - 2];
		}

		for (int i = 0; i < n; i++) {
			System.out.print(x[i] + " ");
		}
	}
}
