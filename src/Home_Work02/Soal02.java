package Home_Work02;

import java.util.Scanner;

public class Soal02 {

	public static void main(String[] args) {
		// Fibonacci 3
		Scanner input = new Scanner(System.in);

		System.out.println("Coba input angka : ");
		int n = input.nextInt();

		int[] x = new int[n];
		x[0] = 1;
		x[1] = 1;
		x[2] = 1;

		for (int i = 3; i < n; i++) {
			x[i] = x[i - 1] + x[i - 2] + x[i - 3];
		}

		for (int i = 0; i < n; i++) {
			System.out.print(x[i] + " ");
		}
	}
}
