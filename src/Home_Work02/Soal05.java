package Home_Work02;

import java.util.Scanner;

public class Soal05 {

	public static void main(String[] args) {
		// Pohon Faktor
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan Angka : ");
		int x = input.nextInt();
		int z = 0;

		for (int i = 2; i <= x; i++) {
			for (int j = 1; j <= i; j++) {
				if (x % i == 0) {
					z = x / i;
					System.out.println(x + "/" + i + "=" + z);
					x = z;
				}
			}
		}
	}
}
