package Home_Work02;

import java.util.*;
import java.text.*;

public class Soal04 {

	public static void main(String[] args) {
		// Conversi Waktu
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan Waktu : ");
		String x = input.nextLine().toLowerCase();

		DateFormat formatAwal = new SimpleDateFormat("hh:mm:ssaa");
		DateFormat formatAkhir = new SimpleDateFormat("HH:mm:ss");

		try {
			Date date = formatAwal.parse(x);
			String output = formatAkhir.format(date);
			System.out.println(output);
		} catch (ParseException z) {
			z.printStackTrace();
		}
	}
}
