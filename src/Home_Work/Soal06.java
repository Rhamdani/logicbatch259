package Home_Work;

import java.util.Iterator;
import java.util.Scanner;

public class Soal06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Coba inputkan suatu kata : ");
		String x = input.nextLine();

		String[] tempSplit = x.split(" ");

		String result = "";

		for (int i = 0; i < tempSplit.length; i++) {
			String[] tempSplit2 = tempSplit[i].split("");
			for (int j = 0; j < tempSplit2.length; j++) {
				if (j % 2 == 1) {
					result += "*";
				} else {
					result += tempSplit2[j];
				}
			}
			result += " ";
		}
		System.out.println(result);
	}
}
