package Home_Work;

import java.util.Arrays;
import java.util.Scanner;

public class Soal05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Coba inputkan suatu kata : ");
		String x = input.nextLine().toLowerCase().replace(" ", "");

		char[] tempArr = x.toCharArray();

		String resultVocal = "";
		String resultKonsonan = "";

		Arrays.sort(tempArr);
		for (int i = 0; i < tempArr.length; i++) {
			if (tempArr[i] == 'a' || tempArr[i] == 'i' || tempArr[i] == 'u' || tempArr[i] == 'e' || tempArr[i] == 'o') {
				resultVocal += tempArr[i];
			} else {
				resultKonsonan += tempArr[i];
			}
		}
		System.out.println("Vocal : " + resultVocal);
		System.out.println("Konsonan : " + resultKonsonan);
		System.out.println(tempArr);
	}
}
