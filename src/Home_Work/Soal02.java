package Home_Work;

import java.util.Iterator;
import java.util.Scanner;

public class Soal02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		int[] arr = new int[5];

		System.out.println("Input Nilai Array : ");
		for (int i = 0; i < arr.length; i++) {
			arr[i] = input.nextInt();
		}

		System.out.println("Rotasi : ");
		int rotasi = input.nextInt();

		int[] x = new int[rotasi];

		for (int i = 0; i < rotasi; i++) {
			x[i] = arr[0];
			for (int j = 0; j < arr.length - 1; j++) {
				arr[j] = arr[j + 1];
				System.out.print(arr[j]);
			}
			arr[arr.length - 1] = x[i];
			System.out.println(x[i]);
		}
	}

}
