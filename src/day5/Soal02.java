package day5;

import java.util.Arrays;
import java.util.Scanner;

public class Soal02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan Sinyal : ");
		String sinyal = input.nextLine().toUpperCase();

		String[] tempSplit = sinyal.split("");
		String result = "";

		int sinyalBenar = 0;
		int sinyalSalah = 0;

		for (int i = 0; i < tempSplit.length; i += 3) {
			result = tempSplit[i] + tempSplit[i + 1] + tempSplit[i + 2];
			if (result.equals("SOS")) {
				sinyalBenar += 1;
			} else {
				sinyalSalah += 1;
			}
		}
		System.out.println("Total sinyal salah : " + sinyalSalah);
	}
}
