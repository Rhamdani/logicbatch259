package day5;

import java.util.Arrays;
import java.util.Scanner;

public class Soal01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan jarak : ");
		String jarak = input.nextLine();
		
		String [] tempSplit = jarak.split("");

		double jarakTempuh = 0;
		double bensin = 0;
		String result = "";

		double[] arrJarak = { 2, 0.5, 1.5, 0.3 };

		for (int i = 0; i < tempSplit.length; i++) {
			jarakTempuh += arrJarak[Integer.parseInt(tempSplit[i])-1];
			result += (arrJarak[Integer.parseInt(tempSplit[i])-1] + "KM " );
		}
		System.out.println("Jarak yang di tempuh " + result + "= " + jarakTempuh);
		bensin = jarakTempuh / 2.5;
		System.out.println("Bensin = " + Math.ceil(bensin) + " Liter");	
	}
}
