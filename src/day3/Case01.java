package day3;

import java.util.Iterator;
import java.util.Scanner;

public class Case01 {

	public static void Resolve1() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan nilai nilai ke 1 = ");
		int n = input.nextInt();

		System.out.print("Masukan nilai nilai ke 2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		int x = 1;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else {
					tempArray[i][j] = x;
					x *= n2;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve2() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan nilai nilai ke 1 = ");
		int n = input.nextInt();

		System.out.print("Masukan nilai nilai ke 2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		int x = 1;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j % 3 == 2) {
					x *= -1;
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
					x *= -n2;
				} else {
					tempArray[i][j] = x;
					x *= n2;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve3() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan nilai nilai ke 1 = ");
		int n = input.nextInt();

		System.out.print("Masukan nilai nilai ke 2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		int x = 1;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j > n / 2) {
					x /= 2;
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
				} else if (x == 1) {
					x *= n2;
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
				} else {
					x *= 2;
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve4() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan nilai nilai ke 1 = ");
		int n = input.nextInt();

		System.out.print("Masukan nilai nilai ke 2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		int x = 1;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j % 2 == 1) {
					tempArray[i][j] = n2;
					System.out.print(tempArray[i][j] + " ");
					n2 += n2;
				} else {
					tempArray[i][j] = x;
					x = x + 1;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve5() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan nilai nilai ke 1 = ");
		int n = input.nextInt();

		int x = 0;

		int[][] tempArray = new int[3][n];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				tempArray[i][j] = x;
				System.out.print(tempArray[i][j] + " ");
				x += 1;
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve6() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan nilai nilai ke 1 = ");
		int n = input.nextInt();

		int x = 1;
		int z = 1;

		int[][] tempArray = new int[3][n];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i == 1) {
					tempArray[i][j] = x;
					x = x * n;
					System.out.print(tempArray[i][j] + " ");
				} else {
					z = tempArray[0][j] + tempArray[1][j];
					tempArray[i][j] = z;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve7() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan nilai nilai ke 1 = ");
		int n = input.nextInt();

		int x = 0;

		int[][] tempArray = new int[3][n];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				tempArray[i][j] = x;
				System.out.print(tempArray[i][j] + " ");
				x += 1;
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve8() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan nilai nilai ke 1 = ");
		int n = input.nextInt();

		int x = 0;
		int z = 0;

		int[][] tempArray = new int[3][n];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i == 1) {
					tempArray[i][j] = x;
					x = x + 2;
					System.out.print(tempArray[i][j] + " ");
				} else {
					z = tempArray[0][j] + tempArray[1][j];
					tempArray[i][j] = z;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve9() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan nilai nilai ke 1 = ");
		int n = input.nextInt();

		System.out.print("Masukan nilai nilai ke 2 = ");
		int n2 = input.nextInt();

		int x = 0;

		int[][] tempArray = new int[3][n];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i == 1) {
					tempArray[i][j] = x;
					x = x + n2;
					System.out.print(tempArray[i][j] + " ");
				} else {
					tempArray[i][j] = x - n2;
					System.out.print(tempArray[i][j] + " ");
					x = x - n2;
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve10() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan nilai nilai ke 1 = ");
		int n = input.nextInt();

		System.out.print("Masukan nilai nilai ke 2 = ");
		int n2 = input.nextInt();

		int x = 0;
		int z = 0;

		int[][] tempArray = new int[3][n];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i == 1) {
					tempArray[i][j] = x;
					x = x + n2;
					System.out.print(tempArray[i][j] + " ");
				} else {
					z = tempArray[0][j] + tempArray[1][j];
					tempArray[i][j] = z;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}

}
