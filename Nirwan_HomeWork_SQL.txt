Home_Work_SQL

//Membuat Database dengan nama DBPenerbit
CREATE DATABASE DBPenerbit

//Membuat Tabel dengan 2 Primary Key
CREATE TABLE tblPengarang (
	ID INT NOT NULL,
    	Kd_Pengarang VARCHAR(7)NOT NULL,
    	Nama VARCHAR(30) NOT NULL,
	Alamat VARCHAR(80) NOT NULL,
	Kota VARCHAR(15) NOT NULL,
	Kelamin VARCHAR(1) NOT NULL,
	PRIMARY KEY (ID, Kd_Pengarang)
)

//Membuat dengan nama vwPengarang dengan attribut Kd_Pengarang, Nama, Kota
CREATE VIEW vwPengarang AS
SELECT Kd_Pengarang, Nama, Kota
FROM tblPengarang

//Memasukan Data ke dalam tabel tblPengarang
INSERT INTO tblPengarang (Kd_Pengarang, Nama, Alamat, Kota, Kelamin)
VALUES ('P0001','Ashadi','Jl. Beo 25','Yogya','P'),
	('P0002','Rian','Jl. Solo 123','Yogya','P'),
	('P0003','Suwadi','Jl. Semangka 13','Bandung','P'),
	('P0004','Siti','Jl. Durian 15','Solo','W'),
	('P0005','Amir','Jl. Gajah 33','Kudus','P'),
	('P0006','Suparman','Jl. Harimau 25','Jakarta','P'),
	('P0007','Jaja','Jl. Singa 7','Bandung','P'),
	('P0008','Saman','Jl. Naga 12','Yogya','P'),
	('P0009','Anwar','Jl. Tidar 6A','Magelang','P'),
	('P0010','Fatmawati','Jl. Renjana 4','Bogor','W')

//Menampilkan Kd_Pengarang dan Nama yang dikelompokan atas Nama
SELECT Kd_Pengarang, Nama
FROM tblPengarang 
ORDER BY Nama

//Menampilkan Kota, Kd_Pengarang dan Nama yang dikelompokan atas kota
SELECT *
FROM vwPengarang
ORDER BY Kota

//Menghitung dan menampilkan jumlah pengarang
SELECT COUNT(Kd_Pengarang) "Jumalah Pengarang"
FROM tblPengarang

//Menampilkan record kota dan jumlah kotanya
SELECT COUNT(ID)"Jumlah Kota", Kota
FROM tblPengarang
GROUP BY Kota

//Menampilkan record kota diatas 1 kota
SELECT COUNT(Kota), Kota
FROM tblPengarang
GROUP BY Kota
HAVING COUNT(Kota) > 1

//Menampilkan Kd_Pengarang yang terbesar dan terkecil
SELECT MAX(Kd_Pengarang)"Terbesar",
MIN(Kd_pengarang)"Terkecil" 
FROM tblPengarang

//Membuat tabel tblGaji
CREATE TABLE tblGaji (
	ID INT PRIMARY KEY NOT NULL,
    	Kd_Pengarang VARCHAR(7) NOT NULL,
   	Nama VARCHAR(30) NOT NULL,
	Gaji INT NOT NULL
)

//Memasukan data kedalam tabel tblGaji
INSERT INTO tblGaji(Kd_Pengarang, Nama, Gaji)
VALUES ('P0002','Rian',600000),
	('P0005','Amir',700000),
	('P0004','Siti',500000),
	('P0003','Suwadi',1000000),
	('P0010','Fatmawati',600000),
	('P0008','Saman',750000)

//Menampilkan gaji tertinggi dan terbesar
SELECT MAX(Gaji)"Gaji Tertinggi",
MIN(Gaji)"Gaji Terendah"
FROM tblGaji

//Menampilakan gaji diatas 600.000
SELECT Gaji
FROM tblGaji
WHERE Gaji > 600000

//Menampilkan jumlah Gahi
SELECT SUM(Gaji)"Jumlah Gaji"
FROM tblGaji

//Menampilkan jumlah gaji berdasarkan kota
SELECT SUM(Gaji), Kota
FROM tblPengarang AS X
JOIN tblGaji AS Z
ON X.ID = Z.ID
GROUP BY P.Kota

//Menampilkan record pengarang antara P0001-P0006
SELECT * 
FROM tblPengarang
WHERE KD_Pengarang BETWEEN 'P0001' AND 'P0006'

//Menampilkan seluruh data Yogya, Solo, dan Magelang
SELECT * 
FROM tblPengarang
WHERE Kota IN ('Yogya', 'Solo', 'Magelang')

//Menampilkan seluruh data yang bukan Yogya
SELECT *
FROM tblPengarang
WHERE Kota NOT LIKE ('Yogya')

//Menampilkan data pengarang yang namanya dimulai dengan huruf [A]
SELECT *
FROM tblPengarang
WHERE Nama LIKE ('A%')

//Menampilkan data pengarang yang namanya berakhiran dengan huruf [i]
SELECT *
FROM tblPengarang
WHERE Nama LIKE ('%i')

//Menampilkan data pengarang dengan huruf ketiganya [a]
SELECT *
FROM tblPengarang
WHERE Nama LIKE ('__a%')

//Menampilkan data pengarang yang namanya tidak berakhiran [n]
SELECT *
FROM tblPengarang
WHERE Nama LIKE '%[!n]'

//Menampilkan seluruh data tabel tblPengarang dan tblGaji dengan Kd_Pengarang yang sama
SELECT *
FROM tblPengarang AS P
JOIN tblGaji AS G
ON P.Kd_Pengarang = G.Kd_Pengarang

//Menampilkan kota yang memiliki gaji dibawah 1.000.000
SELECT Kota
FROM tblPengarang AS P
JOIN tblGaji AS G
ON P.ID = G.ID
WHERE Gaji < 1000000

//Mengubah panjang dan tipe data kelamin menjadi 10
ALTER TABLE tblPengarang ALTER COLUMN Kelamin TYPE varchar (11) ;

//Menambahkan kolom [Gelar] dengan tipe varchar(12) pada tabel tblPengarang
ALTER TABLE tblPengarang
ADD Gelar VARCHAR(12)

//Mengubah alamat dan kota dari Rian di tabel tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekan Baru
UPDATE tblPengarang
SET Alamat = 'Jl. Cendrawasih 65 ', Kota= 'Pekanbaru'
WHERE Nama = 'Rian'