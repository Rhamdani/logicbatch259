//1. Membuat database Kemahasiswaan yang bernama : DBUniversitas
CREATE DATABASE DBUniversitas

//Membuat tabel JurusanKuliah
CREATE TABLE JurusanKuliah (
		ID INT PRIMARY KEY NOT NULL,
    	Kode_Jurusan VARCHAR(12)NOT NULL,
    	Deskripsi VARCHAR(20) NOT NULL
)

//Mengisi data tabel JurusanKuliah
INSERT INTO JurusanKuliah(Kode_Jurusan, Deskripsi)
VALUES ('KA001', 'Teknik Informatika'),
	('KA002', 'Management Bisnis'),
	('KA003', 'Ilmu Komunikasi'),
	('KA004', 'Sastra Inggris'),
	('KA005', 'Ilmu Pengetahuan Alam Dan Informatika'),
	('KA006', 'Kedokteran')

//Membuat tabel Dosen
CREATE TABLE TblDosen (
		ID INT PRIMARY KEY NOT NULL,
    	Kode_Dosen VARCHAR(14)NOT NULL,
    	Nama_Dosen VARCHAR(15) NOT NULL,
		Kode_Mata_Kuliah VARCHAR(12)NOT NULL,
    	Kode_Fakultas VARCHAR(13) NOT NULL
)

//Mengisi data tabel TblDosen
INSERT INTO TblDosen(Kode_Dosen, Nama_Dosen, Kode_Mata_Kuliah, Kode_Fakultas)
VALUES ('GK001', 'Ahmad Presetyo', 'KPK001', 'TK002'),
	('GK002', 'Hadi Fuladi', 'KPK002', 'TK001'),
	('GK003', 'Johan Goerge', 'KPK003', 'TK002'),
	('GK004', 'Bima Darmawan', 'KPK004', 'TK002'),
	('GK005', 'Gatot Wahyudi', 'KPK005', 'TK001')

//Membuat tabel MataKuliah
CREATE TABLE MataKuliah (
		ID INT PRIMARY KEY NOT NULL,
    	Kode_Mata_Kuliah VARCHAR(12)NOT NULL,
    	Nama_Mata_Kuliah VARCHAR(20) NOT NULL,
    	KeaktifanStatus VARCHAR(15) NOT NULL
)

//Mengisi data tabel MataKuliah
INSERT INTO MataKuliah(Kode_Mata_Kuliah, Nama_Mata_Kuliah, KeaktifanStatus)
VALUES ('KPK001', 'Algoritma Dasar', 'Aktif'),
		('KPK002', 'Basis Data', 'Aktif'),
		('KPK003', 'Kalkulus', 'Aktif'),
		('KPK004', 'Pengantar Bisnis', 'Aktif'),
		('KPK005', 'Matematika Ekonomi & Bisnis', 'Non Aktif')

//Membuat tabel TblMahasiswa
CREATE TABLE TblMahasiswa (
		ID INT PRIMARY KEY NOT NULL,
    	Kode_Mahasiswa VARCHAR(12)NOT NULL,
    	Nama_Mahasiswa VARCHAR(20) NOT NULL,
		Nama_Belakang_Mahasiswa VARCHAR(20) NOT NULL,
    	Alamat_Mahasiswa VARCHAR(30) NOT NULL,
		Kode_Jurusan VARCHAR(12)NOT NULL,
		Kode_Mata_Kuliah VARCHAR(12)NOT NULL
)

//Mengisi data tabel TblMahasiswa
INSERT INTO TblMahasiswa(Kode_Mahasiswa, Nama_Mahasiswa, Nama_Belakang_Mahasiswa, Alamat_Mahasiswa, Kode_Jurusan, Kode_Mata_Kuliah)
VALUES ('MK001', 'Rhony', 'Iskandar', 'Jl. Hati besar NO.63 RT.13', 'KA001', 'KPK001'),
		('MK002', 'Fullana', 'Binharjo', 'Jl. Biak kebagusan NO.34', 'KA002', 'KPK002'),
		('MK003', 'Sardine', 'Himura', 'Jl. Kebajian NO.84', 'KA001', 'KPK003'),
		('MK004', 'Isani', 'Isabul', 'Jl. Merak merpati NO.78', 'KA001', 'KPK001'),
		('MK005', 'Charlie', 'Birawa', 'Jl. Air terjun semidi NO.56', 'KA003', 'KPK002')

//Membuat tabel NilaiMahasiswa
CREATE TABLE NilaiMahasiswa (
		ID INT PRIMARY KEY NOT NULL,
		Kode_Nilai VARCHAR(12) NOT NULL,
    	Kode_Mahasiswa VARCHAR(12)NOT NULL,
		Kode_Organisasi VARCHAR(20) NOT NULL,
		Nilai INT NOT NULL
)

//Mengisi data tabel NilaiMahasiswa
INSERT INTO NilaiMahasiswa(Kode_Nilai, Kode_Mahasiswa, Kode_Organisasi, Nilai)
VALUES ('SK001', 'MK004', 'KK001', '90'),
		('SK002', 'MK001', 'KK001', '80'),
		('SK003', 'MK002', 'KK003', '85'),
		('SK004', 'MK004', 'KK002', '95'),
		('SK005', 'MK005', 'KK005', '70')

//Membuat tabel Fakultas
CREATE TABLE TblFakultas (
		ID INT PRIMARY KEY NOT NULL,
		Kode_Fakultas VARCHAR(13) NOT NULL,
		Penjelasan VARCHAR(30) NOT NULL
)

//Mengisi data Fakultas
INSERT INTO TblFakultas(Kode_Fakultas, Penjelasan)
VALUES ('TK001', 'Teknik Informatika'),
		('TK002', 'Matematika'),
		('TK003', 'Sistem Informatika')
		
//Membuat tabel Organisasi
CREATE TABLE TblOrganisasi (
		ID INT PRIMARY KEY NOT NULL,
		Kode_Organisasi VARCHAR(12) NOT NULL,
		Nama_Organisasi VARCHAR(17) NOT NULL,
		Status_Organisasi VARCHAR(13) NOT NULL
)

//Mengisi data Organisasi
INSERT INTO TblOrganisasi(Kode_Organisasi, Nama_Organisasi, Status_Organisasi)
VALUES ('KK001', 'Unit Kegiatan Mahasiswa (UKM)', 'Aktf'),
		('KK002', 'Badan EKsekutif Mahasiswa Fakultas (BEMF)', 'Aktif'),
		('KK003', 'Dewan Perwakilan Mahasiswa Universitas (DPMU)', 'Aktif'),
		('KK004', 'Badan EKsekutif Mahasiswa Universitas (BEMU)', 'Non Aktif'),
		('KK005', 'Himpunan Mahasiswa Jurusan', 'Non Aktif'),
		('KK006', 'Himpunan Kompetisi Jurusan', 'Aktif')
		
//2. Ubah panjang karakter nama dosen menjadi 200
ALTER TABLE tblDosen
ALTER COLUMN Nama_Dosen TYPE VARCHAR(200)
		
//3. Menampilkan kode mahasiswa, nama mahasiswa, nama mata kuliah, deskripsi jurusan kuliah
dengan kode mahasiswa MK001
SELECT M.Kode_Mahasiswa, M.Nama_Mahasiswa, K.Nama_Mata_Kuliah, J.Deskripsi
FROM TblMahasiswa M
JOIN MataKuliah K
ON M.Kode_Mata_Kuliah = K.Kode_Mata_Kuliah
JOIN JurusanKuliah J
ON M.Kode_Jurusan = J.Kode_Jurusan
WHERE M.Kode_Mahasiswa = 'MK001'

//4. Menampilkan nama mata kuliah, status mata kuliah, kode mahasiswa, nama mahasiswa
alamat mahasiswa, kode jurusan, kode mata kuliah status kuliah tidak aktif
SELECT K.Nama_Mata_Kuliah, K.KeaktifanStatus, M.Kode_Mahasiswa, M.Nama_Mahasiswa,
M.Alamat_Mahasiswa, M.Kode_Jurusan, M.Kode_Mata_Kuliah
FROM MataKuliah K
JOIN TblMahasiswa M
ON K.Kode_Mata_Kuliah = M.Kode_Mata_Kuliah
WHERE K.KeaktifanStatus = 'Non Aktif'

//5. Menampilkan kode mahasiswa, nama mahasiswa, alamat mahasiswa
dengan stutus organisasi aktif dan nilai diatas 79
SELECT M.Kode_Mahasiswa, M.Nama_Mahasiswa, M.Alamat_Mahasiswa
FROM TblMahasiswa M
JOIN NilaiMahasiswa N
ON M.Kode_Mahasiswa = N.Kode_Mahasiswa
JOIN TblOrganisasi O
ON N.Kode_Organisasi = O.Kode_Organisasi
WHERE O.Status_Organisasi = 'Aktif'
AND N.Nilai > 79

//6. Menampilkan kode mata kuliah, nama mata kuliah, status mata kuliah
dengan nama mata kuliah mengandung huruf 'n'
SELECT Kode_Mata_Kuliah, Nama_Mata_Kuliah, KeaktifanStatus
FROM MataKuliah
WHERE Nama_Mata_Kuliah LIKE '%n%'

//7. Menampilkan nama mahasiswa yang memiliki organisasi lebih dari 1
SELECT COUNT (O.Kode_Organisasi), M.Nama_Mahasiswa
FROM TblMahasiswa M
JOIN NilaiMahasiswa N
ON M.Kode_Mahasiswa = N.Kode_Mahasiswa
JOIN TblOrganisasi O
ON N.Kode_Organisasi = O.Kode_Organisasi
GROUP BY M.Nama_Mahasiswa
HAVING COUNT(O.Kode_Organisasi) > 1

//8. Menampilkan kode mahasiswa, nama mahasiswa, nama mata kuliah, deskripsi jurusan,
nama dosen, status mata kuliah, deskripsi fakultas dengan kode mahasiswa MK001
SELECT M.Kode_Mahasiswa, M.Nama_Mahasiswa, K.Nama_Mata_Kuliah, J.Deskripsi, D.Nama_Dosen,
K.KeaktifanStatus, F.Penjelasan
FROM TblMahasiswa M
JOIN MataKuliah K
ON M.Kode_Mata_Kuliah = K.Kode_Mata_Kuliah
JOIN JurusanKuliah J
ON M.Kode_Jurusan = J.Kode_Jurusan
JOIN TblDosen D
ON M.Kode_Mata_Kuliah = D.Kode_Mata_Kuliah
JOIN tblFakultas F
ON D.Kode_Fakultas = F.Kode_Fakultas
WHERE M.Kode_Mahasiswa = 'MK001'

//9. Membuat view dengan kode mahasiswa, nama mahasiswa, nama mata kuliah, nama dosen,
status matakuliah, deskripsi fakultas
CREATE VIEW VirtualTabel
AS SELECT M.Kode_Mahasiswa, M.Nama_Mahasiswa, K.Nama_Mata_Kuliah, D.Nama_Dosen,
K.KeaktifanStatus, F.Penjelasan
FROM TblMahasiswa M
JOIN MataKuliah K
ON M.Kode_Mata_Kuliah = K.Kode_Mata_Kuliah
JOIN TblDosen D
ON M.Kode_Mata_Kuliah = D.Kode_Mata_Kuliah
JOIN tblFakultas F
ON D.Kode_Fakultas = F.Kode_Fakultas

//10. Menampilkan seluruh data dari virtual tabel dimana kode mahasiswa MK001
SELECT *
FROM VirtualTabel
WHERE Kode_Mahasiswa = 'MK001'

//11. Membuat kolom kode mahasiswa pada tabel mahasiswa sebagai index
CREATE INDEX indexKodeMahasiswa
ON TblMahasiswa(Kode_Mahasiswa)

//12. Membuat kolom kode nilai pada nilai mahasiswa sebagai kolom Unique
ALTER TABLE NilaiMahasiswa
ADD UNIQUE (Kode_Nilai)
